Happy Penguin: The Next Generation
============

[Discuss this project here.](http://happypenguin.onkoistudios.com)


Getting started
-----



First, get your dev env set up:

1. This is a django  project. If you aren't familiar with these then follow these steps closely.

2. Make sure you have python (i hope so!), and `pip` installed

3.  Get `virtualenv` set up. This is not necessary, but highly recommended, as
it resolves the version conflict issue. It will certainly be in use on
production.

4. Install necessary packages with `pip install -r requirements/local.txt`. If you
are not in a virtualenv, you will need to `sudo` this last command to install
packages on your system globally.

5. To test to make sure everything is running, cd into happypenguin and try
running `python manage.py` and you should get a list of management commands.

6. (optional) get fabric installed for remote administration

Second, get the initial data:

1. Download data: `wget https://bitbucket.org/michaelb/lgt-data-dump/downloads/happypenguin.tar.gzhappypenguin.tar.gz` (I have no idea how the filename got messed up, but I am too lazy to fix)

2. Untar it to some handy location (I untarred it to ./data, such that there
    exists a directory ./data/screenshots)

Third, generate a dev sqlite DB and get hacking!

1. Make sure you have sqlite & python-sqlite installed

2. Create tables: `python mange.py syncdb` -- you will be asked to create a testing admin
account and a domain name.

3. Create more tables: `python mange.py migrate`

4. `python manage.py import_hp_dump data/` where `data/` is where you put the
unzipped data from the previous section.  this will import everything into your
new test db.

5. `python manage.py runserver` to run a temporary server! Go to
`localhost:8000` and check it out :)

Note: there's a handy script: `tools/reset_data.sh`. This will delete all
tome-related data, then re-import it. It is useful if you are changing around
data-models or something.

Handy feature: Try `python manage.py import_hp_dump --sample` to just do 20
games, which will be much faster.



Style info
------

- [Pallet (chosen because of "retro" colors)](http://www.colourlovers.com/palette/53698/Its_a_Virtue)

- [Bootstrap theme (from above color scheme)](http://www.stylebootstrap.info/index.php?style=VMxlFu6B86U54mbXKRjho)

- Background from SubtleBackgrounds


--------------



                   .888888:.
                   88888.888.       ..ooo00000oo..
                  .8888888888    000000000000000000oo..       ..oo00
                  8' `88' `888   0000''' |  /'''00000000000000000000
            HH    8 8 88 8 888   000     | /    00    \  ''00''  000
     HI     |H    8:.,::,.:888   000     |/     00     \ ________000
     HH,    |H   .8`::::::'888   000___    /|   00    /   \      000
     |HH,   |H   88  `::'  888   000  /   / |   00----____/      000
     | HH,  HH  .88        `888. 000 /   /  |   00   /    \      000
     |  YHHHHH.88'   .::.  .:8888.00..ooo0000oo.00  /      \     000
      \    YHH888.'   :'    `'88:88.0000000000000000oo..    \ ..o000
       \==/ .8888'    '        88:88.'  \  |  ''00000000000000000000
        == .8888H,    .        88:888    \ |    00    \   ''   / 000
           `8888HH,   :        8:888'     \|    00___  \  /   /  000
            `.:.8HH,  .       .::888'------o----00  /   \/   /___000
           .:::::88H  `      .:::::::.     /\   00 /             000
          .::::::.8         .:::::::::    /  \  00/    /|   |\   000
          :::::::::..     .:::::::::'..oo00oo.. 00  /|/ |   | \  000
           `:::::::::88888:::::::'00000000000000000oo.. |   | ..oo00
              rs`:::'       `:'  000'''      ' Rafal Slubowski000
                                                   ''00''


