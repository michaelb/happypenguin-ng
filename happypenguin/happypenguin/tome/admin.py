
from copy import deepcopy

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from mezzanine.conf import settings
from mezzanine.core.admin import DisplayableAdmin, OwnableAdmin

from .models import Game, Update, Link

admin.site.register(Link)
admin.site.register(Update)
admin.site.register(Game)

#blogpost_fieldsets = deepcopy(DisplayableAdmin.fieldsets)
#blogpost_fieldsets[0][1]["fields"].insert(1, "categories")
#blogpost_fieldsets[0][1]["fields"].extend(["content", "allow_comments"])
#blogpost_list_display = ["title", "user", "status", "admin_link"]
#if settings.BLOG_USE_FEATURED_IMAGE:
#    blogpost_fieldsets[0][1]["fields"].insert(-2, "featured_image")
#    blogpost_list_display.insert(0, "admin_thumb")
#blogpost_fieldsets = list(blogpost_fieldsets)
#blogpost_fieldsets.insert(1, (_("Other posts"), {
#    "classes": ("collapse-closed",),
#    "fields": ("related_posts",)}))
#blogpost_list_filter = deepcopy(DisplayableAdmin.list_filter) + ("categories",)
#
#
#class GameAdmin(DisplayableAdmin, OwnableAdmin):
#    """
#    Admin class for blog posts.
#    """
#
#    fieldsets = blogpost_fieldsets
#    list_display = blogpost_list_display
#    list_filter = blogpost_list_filter
#    filter_horizontal = ("categories", "related_posts",)
#
#    def save_form(self, request, form, change):
#        """
#        Super class ordering is important here - user must get saved first.
#        """
#        OwnableAdmin.save_form(self, request, form, change)
#        return DisplayableAdmin.save_form(self, request, form, change)
#
#
#admin.site.register(Game, GameAdmin)
