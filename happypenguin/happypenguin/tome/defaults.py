"""
Default settings for the ``tome`` app. Each of these can be
overridden in your project's settings module, just like regular
Django settings. The ``editable`` argument for each controls whether
the setting is editable via Django's admin.

Thought should be given to how a setting is actually used before
making it editable, as it may be inappropriate - for example settings
that are only read during startup shouldn't be editable, since changing
them would require an application reload.
"""

from django.utils.translation import ugettext_lazy as _

from mezzanine.conf import register_setting


register_setting(
    name="TOME_USE_FEATURED_IMAGE",
    description=_("Enable featured images in tome posts"),
    editable=False,
    default=False,
)

register_setting(
    name="TOME_URLS_USE_DATE",
    label=_("Use date URLs"),
    description=_("If ``True``, URLs for tome post include the month and "
        "year. Eg: /tome/yyyy/mm/slug/"),
    editable=False,
    default=False,
)

register_setting(
    name="GAME_PER_PAGE",
    label=_("tome posts per page"),
    description=_("Number of tome posts shown on a tome listing page."),
    editable=True,
    default=5,
)

register_setting(
    name="TOME_RSS_LIMIT",
    label=_("tome posts RSS limit"),
    description=_("Number of most recent tome posts shown in the RSS feed. "
        "Set to ``None`` to display all tome posts in the RSS feed."),
    editable=False,
    default=20,
)

register_setting(
    name="TOME_SLUG",
    description=_("Slug of the page object for the tome."),
    editable=False,
    default="tome",
)
