
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.utils.feedgenerator import Atom1Feed
from django.utils.html import strip_tags

from mezzanine.generic.models import Keyword
from mezzanine.pages.models import Page
from mezzanine.conf import settings
from mezzanine.utils.models import get_user_model

from .models import Game

User = get_user_model()


class PostsRSS(Feed):
    """
    RSS feed for all tome posts.
    """

    def __init__(self, *args, **kwargs):
        """
        Use the title and description of the tome page for the feed's
        title and description. If the tome page has somehow been
        removed, fall back to the ``SITE_TITLE`` and ``SITE_TAGLINE``
        settings.
        """
        self.tag = kwargs.pop("tag", None)
        self.username = kwargs.pop("username", None)
        super(PostsRSS, self).__init__(*args, **kwargs)
        self._public = True
        try:
            page = Page.objects.published().get(slug=settings.tome_SLUG)
        except Page.DoesNotExist:
            page = None
        else:
            self._public = not page.login_required
        if self._public:
            if page is not None:
                self.title = page.title
                self.description = strip_tags(page.description)
            else:
                settings.use_editable()
                self.title = settings.SITE_TITLE
                self.description = settings.SITE_TAGLINE

    def link(self):
        return reverse("tome_post_feed", kwargs={"format": "rss"})

    def items(self):
        if not self._public:
            return []
        tome_posts = Game.objects.published().select_related("user")
        if self.tag:
            tag = get_object_or_404(Keyword, slug=self.tag)
            tome_posts = tome_posts.filter(keywords__in=tag.assignments.all())
        if self.username:
            author = get_object_or_404(User, username=self.username)
            tome_posts = tome_posts.filter(user=author)
        limit = settings.tome_RSS_LIMIT
        if limit is not None:
            tome_posts = tome_posts[:settings.tome_RSS_LIMIT]
        return tome_posts

    def item_description(self, item):
        return item.content

    def item_author_name(self, item):
        return item.user.get_full_name() or item.user.username

    def item_author_link(self, item):
        username = item.user.username
        return reverse("tome_post_list_author", kwargs={"username": username})

    def item_pubdate(self, item):
        return item.publish_date


class PostsAtom(PostsRSS):
    """
    Atom feed for all tome posts.
    """

    feed_type = Atom1Feed

    def subtitle(self):
        return self.description

    def link(self):
        return reverse("tome_post_feed", kwargs={"format": "atom"})
