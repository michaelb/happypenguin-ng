from django import forms

from mezzanine.core.models import CONTENT_STATUS_DRAFT

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from .models import Game

# These fields need to be in the form, hidden, with default values,
# since it posts to the tome post admin, which includes these fields
# and will use empty values instead of the model defaults, without
# these specified.
hidden_field_defaults = ("status", "gen_description")


class GameSubmissionForm(forms.ModelForm):
    """
    Form for submitting game as displayed to user
    """

    class Meta:
        model = Game
        exclude = (
                "url",
                "publish_date",
                "expiry_date",
                "author",
                "slug",
                "short_url",
                "status",
                "user",
                "in_sitemap",
                "_meta_title",
                "gallery",
                "gen_description",
                "description",
            )


    def __init__(self, *a, **k):
        self.helper = FormHelper()
        self.helper.form_class = 'game-submit'
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        super(GameSubmissionForm, self).__init__(*a, **k)

class GameForm(forms.ModelForm):
    """
    Model form for ``Game`` that provides the quick tome panel in the
    admin dashboard.
    """

    class Meta:
        model = Game
        fields = ("title", "content") + hidden_field_defaults

    def __init__(self):
        initial = {}
        for field in hidden_field_defaults:
            initial[field] = Game._meta.get_field(field).default
        initial["status"] = CONTENT_STATUS_DRAFT
        super(GameForm, self).__init__(initial=initial)
        for field in hidden_field_defaults:
            self.fields[field].widget = forms.HiddenInput()

