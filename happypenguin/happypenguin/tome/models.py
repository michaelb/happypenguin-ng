from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

from mezzanine.conf import settings
from mezzanine.core.fields import FileField
from mezzanine.core.models import Displayable, Ownable, RichText, Slugged
from mezzanine.generic.fields import CommentsField, RatingField
from mezzanine.utils.models import AdminThumbMixin, upload_to

from mezzanine.galleries.models import Gallery

class Update(Displayable, Ownable, RichText):
    """
    A tome post.
    """

    game = models.ForeignKey("tome.Game")

    NEWS = "n"
    VERSION = "v"
    INITIAL_RELEASE = "r"
    FUNDRAISER = "f"

    UPDATE_TYPES = (
        (NEWS, "News"),
        (VERSION, "New version release"),
        (INITIAL_RELEASE, "Initial release"),
        (FUNDRAISER, "Fundraiser announcement"),
    )


    type = models.CharField(default=INITIAL_RELEASE, max_length=1,
                            help_text=_("Select the type of update."),
                            choices=UPDATE_TYPES)

    # A link to a relevant external site
    link = models.ForeignKey("tome.Link", null=True, blank=True)

    #comments = CommentsField(verbose_name=_("Comments"))

    version = models.CharField(max_length=64,
                    null=True, blank=True,
                    help_text=_("If this is a version release, enter the version that was released here."))

    #################################################
    # Fields provided by inherited classes          #
    #################################################

    # Ownable ################################
    # user = models.ForeignKey(user_model_name, verbose_name=_("Author"))
    # def is_editable(self, request):

    # Displayable ############################
    # status = models.IntegerField(_("Status"),
    # publish_date = models.DateTimeField(_("Published from"),
    # expiry_date = models.DateTimeField(_("Expires on"),
    # short_url = models.URLField(blank=True, null=True)
    # in_sitemap = models.BooleanField(_("Show in sitemap"), default=True)

    # RichText ###############################
    # content = RichTextField(_("Content"))
    # search_fields = ("content",)

    def get_type_class(self):
        return {
                self.VERSION: 'new-version',
                self.INITIAL_RELEASE: 'game-added',
                self.FUNDRAISER: 'fundraiser',
            }.get(self.type, 'news')


class Link(models.Model):
    WEBSITE = "w"
    DOWNLOAD = "d"
    CONTRIBUTE = "c"
    DOCUMENTATION = "dc"
    WIKI = "wk"
    FORUM = "fo"

    GIT = "g"
    MERCURIAL = "m"
    REPO = "r"
    SOURCE = "s"

    KICKSTARTER = "ks"
    INDIE_GO_GO = "gg"
    WEPAY = "wp"
    FUNDRAISER = "fr"

    FLATTR = "fl"
    GITTIP = "gt"
    PAYPAL = "pp"
    DONATION = "do"


    STEAM_API = "sa"
    STEAM_PAGE = "sp"

    DESURA_API = "da"
    DESURA_PAGE = "dp"

    AUR_API = "aa"
    AUR_PAGE = "ap"

    UBUNTU_SOFTWARE_CENTER_API = "ua"
    UBUNTU_SOFTWARE_CENTER_PAGE = "up"
    PPA = "pa"

    PURCHASE = "pu"

    LINK_TYPES = (
        ("Website links", (
                (WEBSITE, 'Website'), 
                (DOWNLOAD, 'Download'), 
                (CONTRIBUTE, 'Contribute'), 
            )
        ),
        ("Information links", (
                (DOCUMENTATION, 'Documentation'), 
                (FORUM, 'Forum'), 
                (WIKI, 'Wiki'), 
            )
        ),
        ("Source links", (
                (GIT, 'Git'), 
                (MERCURIAL, 'Mercurial'), 
                (REPO, 'Other Repo'), 
                (SOURCE, 'Non-VCS source'), 
            )
        ),
        ("Fundraiser Links", (
                (KICKSTARTER, 'Kickstarter'), 
                (INDIE_GO_GO, 'IndieGoGo'), 
                (WEPAY, 'WePay'), 
                (FUNDRAISER, 'Other Fundraiser'), 
            )
        ),
        ("Donate Links", (
                (FLATTR, 'Flattr'), 
                (GITTIP, 'GitTip'), 
                (PAYPAL, 'Pay Pal'), 
                (DONATION, 'Other Donation'), 
            )
        ),
        ("API Links", (
                (STEAM_API, 'Steam API Link'), 
                (DESURA_API, 'Desura API Link'), 
                (AUR_API, 'AUR API'), 
                (UBUNTU_SOFTWARE_CENTER_API, 'Ubuntu Software Center API'), 
            )
        ),
        ("Distro Links", (
                (UBUNTU_SOFTWARE_CENTER_PAGE, 'Ubuntu Software Center Page'), 
                (AUR_PAGE, 'AUR Page'), 
                (PPA, 'Ubuntu PPA'), 
            )
        ),
        ("Store Links", (
                (STEAM_PAGE, 'Steam page'),
                (DESURA_PAGE, 'Desura page'),
                (PURCHASE, 'Purchase'), 
            )
        ),
    )

    game = models.ForeignKey("tome.Game", related_name="links")
    url = models.URLField(help_text=_("URL"))
    type = models.CharField(default="w", max_length=2,
                            help_text=_("Select what type of resource this link points to."),
                            choices=LINK_TYPES)

    # Is this the primary link for this game?
    primary = models.BooleanField(default=False)

    description = models.CharField(null=True, blank=True, max_length=64,
                            help_text=_("Enter a custom text description for this link (optional)."))

    def is_download(self):
        return self.type in (self.DOWNLOAD, )

    def is_source(self):
        return self.type in (self.GIT, self.MERCURIAL, self.REPO, self.SOURCE)

    def is_purchase(self):
        return self.type in (self.PURCHASE, self.STEAM_PAGE, self.DESURA_PAGE, self.STEAM_API, self.DESURA_API)

    def is_git(self):
        return self.type in (self.GIT, )

    def is_social(self):
        return self.type in (self.FORUM, )

    def is_documentation(self):
        return self.type in (self.DOCUMENTATION, self.WIKI)

    class Meta:
        verbose_name = _("External Link")
        verbose_name_plural = _("External Links")
        ordering = ("-primary",)


class Game(Displayable, Ownable, RichText, AdminThumbMixin):
    """
    A tome entry.
    """

    engine_is_free = models.BooleanField(default=False,
                        help_text=_("The engine is free / open source under a FSF approved  license (eg GPL, MIT)"))
    content_is_free = models.BooleanField(default=False,
                        help_text=_("The game content is free under a FSF approved license"))

    latest_version = models.CharField(max_length=64, help_text=_("Enter the name of the latest version (eg 1.0.4)"), )

    # game.user is who submitted, game.approved_by is moderator who approved it
    approved_by = models.ForeignKey(User,
                        editable=False,
                        help_text=_("Moderator who approved this game for publication."))

    # When first was submitted
    submission_date = models.DateTimeField(auto_now_add=True)

    # When last updated
    update_date = models.DateTimeField(auto_now=True)
    cost = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=6, default="0.0",
                help_text="Cost in USD")

    # Threaded discussion about game
    comments = CommentsField(verbose_name=_("Comments"))

    # Game rating
    rating = RatingField(verbose_name=_("Rating"))

    # Featured screenshot for game
    featured_image = FileField(verbose_name=_("Featured Image"),
        upload_to=upload_to("tome.Game.featured_image", "tome"),
        format="Image", max_length=255, null=True, blank=True)

    admin_thumb_field = "featured_image"

    gallery = models.ForeignKey(Gallery,
                            null=True,
                            help_text=_("Screenshot gallery for this game"),
                            verbose_name=_("Screenshots"))

    def is_happy_penguin(self):
        return 'linux gam' in self.title.lower() and 'tome' in self.title.lower()

    @classmethod
    def create(cls, urls=[], **k):
        """
        Simple helper method to create the game
        """
        game = Game.objects.create(**k)
        if urls:
            for url_k in urls:
                url_k['game'] = game
                Link.objects.create(**url_k)
        return game

    #################################################
    # Fields provided by inherited classes          #
    #################################################

    # Ownable ################################
    # user = models.ForeignKey(user_model_name, verbose_name=_("Author"))
    # def is_editable(self, request):

    # Displayable ############################
    # status = models.IntegerField(_("Status"),
    # publish_date = models.DateTimeField(_("Published from"),
    # expiry_date = models.DateTimeField(_("Expires on"),
    # short_url = models.URLField(blank=True, null=True)
    # in_sitemap = models.BooleanField(_("Show in sitemap"), default=True)

    # RichText ###############################
    # content = RichTextField(_("Content"))
    # search_fields = ("content",)

    class Meta:
        verbose_name = _("Game entry")
        verbose_name_plural = _("Game entrys")
        ordering = ("-publish_date",)

    @models.permalink
    def get_absolute_url(self):
        url_name = "game_detail"
        kwargs = {"slug": self.slug}
        if settings.TOME_URLS_USE_DATE:
            url_name = "game_detail_date"
            month = str(self.publish_date.month)
            if len(month) == 1:
                month = "0" + month
            day = str(self.publish_date.day)
            if len(day) == 1:
                day = "0" + day
            kwargs.update({
                "day": day,
                "month": month,
                "year": self.publish_date.year,
            })
        return (url_name, (), kwargs)


