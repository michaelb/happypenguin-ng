from calendar import month_name

from django.http import Http404
from django.shortcuts import get_object_or_404

from mezzanine.conf import settings
from mezzanine.generic.models import Keyword
from mezzanine.utils.views import render, paginate
from mezzanine.utils.models import get_user_model

from tome.models import Update, Update
from tome.feeds import PostsRSS, PostsAtom

User = get_user_model()



############################################
# Listing by update as opposed to games
############################################

def update_list(request, tag=None, year=None, month=None, username=None,
                   category=None, template="tome/update_list.html"):
    """
    This is the main view, a list of all updates. Note that when a game is
    released, in addition to having a game entry, it should have an "initial
    release" update.

    Display a list of tome posts that are filtered by tag, year, month,
    author or category. Custom templates are checked for using the name
    ``tome/update_list_XXX.html`` where ``XXX`` is the author's username if
    given.
    """
    settings.use_editable()
    templates = []
    updates = Update.objects.published(for_user=request.user)

    if tag is not None:
        tag = get_object_or_404(Keyword, slug=tag)
        updates = updates.filter(keywords__in=tag.assignments.all())
    if year is not None:
        updates = updates.filter(publish_date__year=year)
        if month is not None:
            updates = updates.filter(publish_date__month=month)
            month = month_name[int(month)]

    author = None
    if username is not None:
        author = get_object_or_404(User, username=username)
        updates = updates.filter(user=author)
        templates.append(u"tome/update_list_%s.html" % username)

    prefetch = ("game",)
    updates = updates.select_related("user").prefetch_related(*prefetch)
    updates = paginate(updates, request.GET.get("page", 1),
                          settings.GAME_PER_PAGE,
                          settings.MAX_PAGING_LINKS)
    context = {"updates": updates, "year": year, "month": month,
               "tag": tag, "category": category, "author": author}
    templates.append(template)
    return render(request, templates, context)

#def update_detail(request, slug, year=None, month=None, day=None,
#                     template="tome/update_detail.html"):
#    """. Custom templates are checked for using the name
#    ``tome/update_detail_XXX.html`` where ``XXX`` is the tome
#    posts's slug.
#    """
#    updates = Update.objects.published(for_user=request.user).select_related()
#    update = get_object_or_404(updates, slug=slug)
#    context = {"update": update, "editable_obj": update}
#    templates = [u"tome/update_detail_%s.html" % unicode(slug), template]
#    return render(request, templates, context)


def update_feed(request, format, **kwargs):
    """
    tome posts feeds - maps format to the correct feed view.
    """
    try:
        return {"rss": PostsRSS, "atom": PostsAtom}[format](**kwargs)(request)
    except KeyError:
        raise Http404()
