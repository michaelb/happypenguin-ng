
from django.conf.urls import patterns, url

from mezzanine.conf import settings


# Game patterns.
urlpatterns = patterns("tome.views",
    url("^game/feeds/(?P<format>.*)/$", "game_feed", name="game_feed"),
    url("^game/tag/(?P<tag>.*)/feeds/(?P<format>.*)/$", "game_feed", name="game_feed_tag"),
    url("^game/tag/(?P<tag>.*)/$", "game_list", name="game_list_tag"),
    # url("^category/(?P<category>.*)/feeds/(?P<format>.*)/$", "game_feed", name="game_feed_category"),
    # url("^category/(?P<category>.*)/$", "game_list", name="game_list_category"),

    # By developer (ie author)
    url("^game/developer/(?P<username>.*)/feeds/(?P<format>.*)/$", "game_feed", name="game_feed_author"),
    url("^game/developer/(?P<username>.*)/$", "game_list", name="game_list_author"),

    # Archived by first release date
    url("^game/archive/(?P<year>\d{4})/(?P<month>\d{1,2})/$", "game_list", name="game_list_month"),
    url("^game/archive/(?P<year>.*)/$", "game_list", name="game_list_year"),

    url("^games/$", "game_list", name="game_list"),

    url("^feeds/(?P<format>.*)/$", "update_feed", name="update_feed"),
    url("^tag/(?P<tag>.*)/feeds/(?P<format>.*)/$", "update_feed", name="update_feed_tag"),
    url("^tag/(?P<tag>.*)/$", "update_list", name="update_list_tag"),

    # Archived by first release date
    url("^archive/(?P<year>\d{4})/(?P<month>\d{1,2})/$", "update_list", name="update_list_month"),
    url("^archive/(?P<year>.*)/$", "update_list", name="update_list_year"),

    # Archived by first release date
    url("^submit/$", "game_submit", name="game_submit"),


    #url("^(?P<slug>.*)/$", "update_detail", name="update_detail"),

    url("^$", "update_list", name="home"),

    #url("^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/"
    #    "(?P<slug>.*)/$", "game_detail", name="game_detail_date"),
    url("^(?P<slug>.*)/$", "game_detail", name="game_detail"),


)
