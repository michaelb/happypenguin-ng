import sys

####################################################################
# Terminal output functions
# A few functions for making output to the terminal neater
####################################################################
class Term:
    bold = "\033[1m"
    reset= "\033[0;0m"
    purple = '\033[95m'
    blue = '\033[94m'
    green = '\033[92m'
    yellow = '\033[93m'
    red = '\033[91m'
    Bold = staticmethod(lambda s: Term.bold + s + Term.reset)
    Blue = staticmethod(lambda s: Term.blue + s + Term.reset)
    Yellow = staticmethod(lambda s: Term.yellow + s + Term.reset)
    Green = staticmethod(lambda s: Term.green + s + Term.reset)
    Red = staticmethod(lambda s: Term.red + s + Term.reset)
    Purple = staticmethod(lambda s: Term.purple + s + Term.reset)

def warning(msg):
    sys.stderr.write(Term.Yellow("Warning: ") + Term.Bold(msg) + "\n")

def trace(msg):
    sys.stdout.write(Term.Blue("---> ") + msg + "\n")


def error(msg):
    sys.stderr.write("\n-----------------------------\n")
    sys.stderr.write(Term.Red("Fatal Error: ") +
            Term.Bold(msg) + "\n")
    sys.exit(1)

