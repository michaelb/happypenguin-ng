from calendar import month_name
from django.contrib.auth.decorators import login_required

from django.http import Http404
from django.shortcuts import get_object_or_404

from mezzanine.conf import settings
from mezzanine.generic.models import Keyword
from mezzanine.utils.views import render, paginate
from mezzanine.utils.models import get_user_model

from tome.models import Game, Update
from tome.feeds import PostsRSS, PostsAtom

User = get_user_model()

from update_views import *

from forms import GameSubmissionForm


#@login_required
def game_submit(request, template="tome/game_submit.html"):

    if request.method == "POST":
        form = GameSubmissionForm(request.POST)
        if form.is_valid():
            new_game = form.save(commit=False)

            new_game.user = request.user
            # TODO Fill in other stuff here

            new_game.save()

            # Redirect back to the game detail info
            return redirect(game_detail, new_game.slug)

    else:
        form = GameSubmissionForm()

    context = {
        'form': form,
    }
    return render(request, template, context)




############################################
# Listing by game as opposed to updates
############################################

def game_list(request, tag=None, year=None, month=None, username=None,
                   category=None, template="tome/game_list.html"):
    """
    Display a list of tome posts that are filtered by tag, year, month,
    author or category. Custom templates are checked for using the name
    ``tome/game_list_XXX.html`` where ``XXX`` is the author's username if
    given.
    """
    settings.use_editable()
    templates = []
    games = Game.objects.published(for_user=request.user)
    if tag is not None:
        tag = get_object_or_404(Keyword, slug=tag)
        games = games.filter(keywords__in=tag.assignments.all())
    if year is not None:
        games = games.filter(publish_date__year=year)
        if month is not None:
            games = games.filter(publish_date__month=month)
            month = month_name[int(month)]
    #if category is not None:
    #    category = get_object_or_404(tomeCategory, slug=category)
    #    games = games.filter(categories=category)
    #    templates.append(u"tome/game_list_%s.html" %
    #                      unicode(category.slug))
    author = None
    if username is not None:
        author = get_object_or_404(User, username=username)
        games = games.filter(user=author)
        templates.append(u"tome/game_list_%s.html" % username)

    prefetch = ("links", "keywords__keyword")
    games = games.select_related("user").prefetch_related(*prefetch)
    games = paginate(games, request.GET.get("page", 1),
                          settings.GAME_PER_PAGE,
                          settings.MAX_PAGING_LINKS)
    context = {"games": games, "year": year, "month": month,
               "tag": tag, "category": category, "author": author}
    templates.append(template)
    return render(request, templates, context)


def game_detail(request, slug, year=None, month=None, day=None,
                     template="tome/game_detail.html"):
    """. Custom templates are checked for using the name
    ``tome/game_detail_XXX.html`` where ``XXX`` is the tome
    posts's slug.
    """
    games = Game.objects.published(for_user=request.user).select_related()
    game = get_object_or_404(games, slug=slug)
    updates = Update.objects.filter(game=game).select_related()

    # Paginate updates
    updates = paginate(updates, request.GET.get("page", 1),
                          settings.GAME_PER_PAGE,
                          settings.MAX_PAGING_LINKS)

    context = {"game": game, "editable_obj": game, "updates": updates}
    templates = [u"tome/game_detail_%s.html" % unicode(slug), template]
    return render(request, templates, context)


def game_feed(request, format, **kwargs):
    """
    tome posts feeds - maps format to the correct feed view.
    """
    try:
        return {"rss": PostsRSS, "atom": PostsAtom}[format](**kwargs)(request)
    except KeyError:
        raise Http404()
