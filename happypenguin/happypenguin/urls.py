from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url("^$", "tome.views.update_list", name="home"),

    # First the admin
    url(r'^admin/', include(admin.site.urls)),
)



if settings.IS_DEV:
    urlpatterns += patterns('',
        ###########################
        # Debug stuff
        (r'media/(?P<path>.+)$', 'django.views.static.serve', 
                    {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        (r'static/(?P<path>.+)$', 'django.views.static.serve', 
                    {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
        ###########################
    )


urlpatterns += patterns('',

    # Now include tome urls
    ("^tome/", include("tome.urls")),

    # Finally other mezzanine URLs
    ("^", include("mezzanine.urls")),

)
