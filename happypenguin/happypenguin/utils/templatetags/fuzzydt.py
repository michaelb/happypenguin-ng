from django import template
from django.utils.translation import ungettext, ugettext as _
import datetime
from django.utils.timezone import utc

register = template.Library()

count_words = {
    1: "one", 2: "two", 3: "three", 4: "four", 5: "five", 6: "six", 7:
    "seven", 8: "eight", 9: "nine", 10: "ten", 11: "eleven", 12: "twelve",
    13: "thirteen", 14: "fourteen", 15: "fifteen", 16: "sixteen", 17:
    "seventeen", 18: "eighteen", 19: "nineteen", 20: "twenty",
}


@register.filter
def fuzzy_datetime(d, minimum=""):
    if isinstance(d, basestring):
        return d
    now = datetime.datetime.utcnow().replace(tzinfo=utc)
    today = datetime.datetime(now.year, now.month, now.day).replace(tzinfo=utc)
    delta = now - d
    delta_midnight = today - d
    days = delta.days
    hours = round(delta.seconds / 3600., 0)
    minutes = round(delta.seconds / 60., 0)
    chunks = (
        (365.0, lambda n: ungettext('year', 'years', n)),
        (30.0, lambda n: ungettext('month', 'months', n)),
        (7.0, lambda n : ungettext('week', 'weeks', n)),
        (1.0, lambda n : ungettext('day', 'days', n)),
    )
    
    if days == 0 and "d" not in minimum:
        if hours == 0:
            if minutes > 0:
                return ungettext('1 minute ago', \
                    '%(minutes)d minutes ago', minutes) % \
                    {'minutes': minutes}
            else:
                return _("less than 1 minute ago")
        else:
            return ungettext('1 hour ago', '%(hours)d hours ago', hours) \
            % {'hours':hours}
    elif days == 0:
        return _('today')

    if delta_midnight.days == 0 and "d" not in minimum:
        return _("yesterday at %s") % d.strftime("%H:%M")
    elif delta_midnight.days == 0:
        return _("yesterday")

    count = 0
    for i, (chunk, name) in enumerate(chunks):
        if days >= chunk:
            count = round((delta_midnight.days + 1)/chunk, 0)
            break
    
    str_count = "%d" % count
    if "f" in minimum:
        if count == 2:
            # very fuzzy
            str_count = _("A couple")
        elif count == 3:
            # very fuzzy
            str_count = _("A few")
        elif count in count_words:
            str_count = _(count_words[count])

    return _('%(number)s %(type)s ago') % \
        {'number': str_count, 'type': name(count)}


