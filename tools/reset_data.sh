#!/bin/bash

set -e # die on err

MY_PATH="`dirname \"$0\"`"              # relative
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized

# up one
cd ../happypenguin

# Clear tome data
echo "Clearing Tome data"
python manage.py sqlclear tome | python manage.py dbshell

echo "Rebuilding models"
python manage.py syncdb
python manage.py migrate

echo "Importing data"
python manage.py import_hp_dump ./data $1 $2 $3


